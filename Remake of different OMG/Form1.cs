﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace Remake_of_different_OMG
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Process.Start("https://support.microsoft.com/en-us/windows/get-windows-media-player-81718e0d-cfce-25b1-aee3-94596b658287");
            Application.Exit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form2 main = new Form2();
            this.WindowState = FormWindowState.Minimized;
            main.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Directory.Exists("C:\\Program Files (x86)\\Windows Media Player"))
            {
                Form2 main = new Form2();
                this.WindowState = FormWindowState.Minimized;
                main.Show();
            }
        }
    }
}
